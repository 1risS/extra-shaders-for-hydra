// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-wave.js")

squ(7,0.02).modulateRotate(o1).out(o0)
tri(2).modulateRotate(o0).out(o1)
harmonic(11, 0.01).modulateRotate(o3).out(o2)
saw(3,0.2).modulateScale(o2).colorama(0.05).out(o3)
render()
