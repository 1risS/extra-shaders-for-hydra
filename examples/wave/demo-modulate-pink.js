 // licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-wave.js")

harmonic(10, 0.31).colorama(0.5).modulateRotate(saw(10, -0.17).modulate(rtri(3, 0.11))).out()
