// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-wave.js")

// tri(10, 0).out(o3)
// harmonic(20, 0.1).out(o2)
squ(10, 0.1).out(o0)
tri(10, 0.1).out(o1)
harmonic(10, 0.1, ()=> { return 0.05*Math.sin(0.5*time); }, ()=> { return 0.10*Math.sin(time); }).out(o2)
saw(10, 0.1).out(o3)
render()
