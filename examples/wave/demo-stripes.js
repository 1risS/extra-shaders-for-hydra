// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-wave.js")

stripes(10, 1.1).rotate().out(o0)
rstripes(10, 1.31).out(o1)
src(o1).modulateRepeat(o1).out(o2)
src(o0).modulateScrollX(o1).out(o3)
render()
